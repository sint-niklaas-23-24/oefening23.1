﻿using System;
using System.Windows;

namespace WpfProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Random random = new Random();
        private void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            int g1 = random.Next();
            int g2 = random.Next();

            lblGetal1.Content = g1.ToString();
            lblGetal2.Content = g2.ToString();
        }
    }
}
